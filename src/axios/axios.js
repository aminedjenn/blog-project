import axios from "axios";
import { TOKEN } from "../utils/const";

export const getRequest = async (options, setData = null) => {
  const option = {
    method: options.method,
    request: options.request, // exemple : posts/25/comments
    params: {
      limit: options.params.limit || 10,
      offset: options.params.offset || 0,
    },
  };
  return await axios({
    method: option.method,
    url: `https://supdevinci.nine1000.tech/${option.request}?limit=${option.params.limit}&offset=${option.params.offset}`,
    headers: {
      "x-token": TOKEN,
    },
  });
};

export const postRequest = (request, firstId, content, secondId = null) => {
  axios({
    method: "post",
    url: `https://supdevinci.nine1000.tech/${request}/${firstId}/comments`,
    headers: {
      "x-token": TOKEN,
    },
    data: {
      content: `${content}`,
    },
  })
    .then((res) => {
      console.log(res);
    })
    .catch(function (error) {
      console.log(error);
    });
};
