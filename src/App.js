import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./componants/Home";
import Contact from "./componants/Contact";
import About from "./componants/About";
import AuthorsList from "./componants/Authors";
import { DetailPost } from "./componants/DetailPost";
import Header from "./componants/Header";
import Footer from "./componants/Footer";
import PostsByAuthor from "./componants/PostsByAuthor";

function App() {
  return (
    <Router>
      <div>
        <Header></Header>
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/about" exact component={About}></Route>
          <Route path="/authors" exact component={AuthorsList}></Route>
          <Route path="/contact" exact component={Contact}></Route>
          <Route path="/post/:id" component={DetailPost}></Route>
          <Route path="/author/:id" component={PostsByAuthor}></Route>
        </Switch>
        <Footer></Footer>
      </div>
    </Router>
  );
}

export default App;
