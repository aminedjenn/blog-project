import React from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

export const StyledSpan = styled.span`
  color: grey;
  font-size: 0.8em;
`;

const StyledH2 = styled.h2`
  margin: auto;
`;

const StyledDiv = styled.div`
  padding: 25px;
`;

export const Post = (props) => {
  const { id, date, title } = props;
  return (
    <StyledDiv>
      <StyledH2 {...props}>
        <Link date={date} to={`/post/${id}`}>
          {title}
        </Link>
      </StyledH2>
      <StyledSpan>{date}</StyledSpan>
    </StyledDiv>
  );
};
