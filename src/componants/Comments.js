import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import { useParams } from "react-router-dom";
import ReactMarkdown from "react-markdown";
import dateFormat from "dateformat";

import { StyledSpan } from "./Post";
import { getRequest } from "../axios/axios";

const StyledComment = styled.div`
  display: flex;
  justify-content: left;
  width: 100%;
`;

const CommentContent = styled.div`
  flex: 1 1 0;
  border-top: 1px solid grey;
`;

const CommentStyle = styled.div`
  display: flex;
  justify-content: space-between;

  & > div {
    flex: 1 1 0;
  }
`;
const CountDiv = styled.div`
  font-size: 1.5em;
  margin: 15px;
`;
const Content = styled(CommentContent)`
  background-color: #cecece;
  margin: 10px 10px 10px 0;
  padding: 15px;
  flex-direction: column;
`;

export const Author = (props) => {
  const [authors, setAuthors] = useState({});
  const { idauthor } = props;

  const option = {
    method: "get",
    request: `authors/${idauthor}`,
    params: {
      limit: 100,
    },
  };
  useEffect(() => {
    getRequest(option).then((res) => {
      const authorsList = res.data;
      setAuthors(authorsList);
      return authors;
    });
  }, []);

  return (
    <>
      <div {...props} key={authors.id}>
        {authors.display_name}
      </div>
    </>
  );
};
const Comments = (props) => {
  const postId = useParams().id;
  const [comments, setComments] = useState([]);

  useEffect(() => {
    let mounted = true;
    const option = {
      method: "get",
      request: `posts/${postId}/comments`,
      params: { limit: 100 },
    };
    getRequest(option).then((res) => {
      if (mounted) {
        const commentsList = res.data;
        setComments(commentsList.result);
      }
    });
    return () => (mounted = false);
  }, [comments, postId]);

  return (
    <StyledComment {...props}>
      <CommentContent>
        {
          <CountDiv>
            {comments.length ? `${comments.length}` : "No"} comment(s).
          </CountDiv>
        }
        {comments.map((comment) => {
          const date = dateFormat(
            comment.created_at,
            "ddd mmm dd yyyy HH:MM:ss"
          );
          return (
            <Content key={comment.id}>
              <CommentStyle>
                <Author idauthor={comment.author}></Author>
                <div>
                  <StyledSpan>{date}</StyledSpan>
                </div>
              </CommentStyle>
              <Content>
                <ReactMarkdown>{comment.content}</ReactMarkdown>
              </Content>
            </Content>
          );
        })}
      </CommentContent>
    </StyledComment>
  );
};

export default Comments;
