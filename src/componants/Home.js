import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import dateFormat from "dateformat";

import { Post } from "./Post";
import { getRequest } from "../axios/axios";

export const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Home = (props) => {
  const [posts, setPosts] = useState([]);
  const option = {
    method: "get",
    request: `posts`,
    params: { limit: 100 },
  };

  useEffect(() => {
    getRequest(option).then((res) => {
      const postsList = res.data.result;
      setPosts(postsList);
      return posts;
    });
  }, []);
  return (
    <StyledDiv {...props}>
      <h1>Latest Posts</h1>
      {posts.map((post) => {
        const date = post.created_at;
        const formatedDate = dateFormat(date, "ddd mmm dd yyyy HH:MM:ss");
        return (
          <Post
            key={post.id}
            title={post.title}
            date={formatedDate}
            id={post.id}
          ></Post>
        );
      })}
    </StyledDiv>
  );
};

export default Home;
