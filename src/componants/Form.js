import React, { useRef, useState } from "react";
import styled from "@emotion/styled";
import { useParams } from "react-router-dom";

import { postRequest } from "../axios/axios";

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 30px 0 30px 0;
  & > label > textarea {
    width: 100%;
  }
  & > input {
    width: 10em;
    margin-top: 10px;
    padding: 1em;
    background-color: #ff2727;
    border: 2px solid;
    color: white;
    border-radius: 5px;
    text-transform: uppercase;
    transition: 0.2s ease;
  }
  & > input:hover {
    background-color: #d00000;
    box-shadow: 3px 3px 4px #8e8e8e;
    cursor: pointer;
  }
  & > input:active {
    background-color: blue;
    cursor: pointer;
    transform: translateY(2px);
  }
  & > textarea {
    transition: 180ms box-shadow ease-in-out;
    border: 2px solid #ee6e73;
    border-radius: 0;
  }
`;

const StyledTextarea = styled.textarea`
  :focus {
    background-color: #f1f1f1;
  }
`;

export const Form = (props) => {
  const textInput = useRef(null);
  const [comment, setComment] = useState("");
  const { id } = useParams();

  const handleChange = (event) => {
    setComment({ value: event.target.value });
  };

  const handleSubmit = (event) => {
    postRequest("posts", id, comment.value);
    event.preventDefault();
    textInput.current.value = "";
  };
  return (
    <StyledForm {...props} onSubmit={handleSubmit}>
      <StyledTextarea
        ref={textInput}
        rows="10"
        placeholder="Enter a comment"
        value={comment.value}
        onChange={handleChange}
      />
      <input type="submit" value="Envoyer" />
    </StyledForm>
  );
};
