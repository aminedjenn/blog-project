import React from "react";
import { Nav, NavBrand, Li } from "./Nav";
import { Link } from "react-router-dom";

const Header = () => {
  const navLinks = [
    { path: "/about", component: "About", name: "About Us" },
    { path: "/authors", component: "Authors", name: "Our Authors" },
    { path: "/contact", component: "Contact", name: "Contact Us" },
  ];
  return (
    <header>
      <Nav>
        <NavBrand>
          <Link to="/">Sup de vinci</Link>
        </NavBrand>
        {navLinks.map((link) => {
          return (
            <Li key={`${link.path}`}>
              <Link to={link.path}>{link.name}</Link>
            </Li>
          );
        })}
      </Nav>
    </header>
  );
};

export default Header;
