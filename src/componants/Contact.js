import React from "react";
import styled from "@emotion/styled";

const StyledContact = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 30px 20% 50px 20%;
`;
const Title = styled.h2`
  text-align: center;
`;

const Contact = () => {
  return (
    <StyledContact>
      <Title>Contact us</Title>
      lorem ipsum dolor sit amet, consectetur adipiscing elit. In nunc tortor,
      consequat vel pharetra nec, mattis sit amet lorem. Suspendisse et dictum
      sapien. Aenean sollicitudin dolor id odio sollicitudin dignissim. Integer
      laoreet laoreet purus, ut fringilla neque ultrices eget. Quisque aliquet
      lectus non elit mattis, nec cursus massa viverra. Nullam non orci nulla.
      In hac habitasse platea dictumst. In id auctor ante. Aliquam porttitor leo
      a orci pharetra condimentum. Mauris vel eros ex. Donec neque ante,
      tristique vitae diam vel, efficitur malesuada libero. Sed sagittis metus
      augue, id porttitor est sagittis et. Duis congue tortor magna, dictum
      tristique ligula consequat eget. Donec efficitur sit amet eros et ornare.
      Nulla ultrices tortor ante, placerat malesuada ex facilisis ac.
    </StyledContact>
  );
};

export default Contact;
