import React from "react";
import styled from "@emotion/styled";

const StyedFooter = styled.footer`
  text-align: center;
`;
const StyledSpan = styled.span`
  font-weight: bold;
`;

const Footer = () => {
  const date = new Date();
  return (
    <div>
      <hr></hr>
      <StyedFooter>
        <StyledSpan>Sup de vinci</StyledSpan> - All rights reserved{" "}
        {date.getFullYear()}
      </StyedFooter>
    </div>
  );
};

export default Footer;
