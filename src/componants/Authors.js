import React, { useEffect, useState } from "react";
import { getRequest } from "../axios/axios";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const StyledAuthorsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const StyledAuthor = styled.div`
  flex: 1 1 100%;
  padding: 30px;
  border: 2px solid blue;
  margin: 20px;
  text-align: center;
`;

const Title = styled.h1`
  text-align: center;
`;

export const Authors = () => {
  const [authors, setAuthors] = useState([]);
  const option = {
    method: "get",
    request: "authors",
    params: {
      limit: 100,
    },
  };
  useEffect(() => {
    getRequest(option).then((res) => {
      const authorsList = res.data.result;
      setAuthors(authorsList);
      return authors;
    });
  }, []);
  return (
    <StyledAuthorsContainer>
      {authors.map((author) => {
        return (
          <div key={author.id} style={{ width: "50%" }}>
            <StyledAuthor>
              <Link to={`author/${author.id}`}>
                Show Post(s) from {author.display_name}
              </Link>
            </StyledAuthor>
          </div>
        );
      })}
    </StyledAuthorsContainer>
  );
};

const AuthorsList = (props) => {
  return (
    <>
      <Title>Our authors</Title>
      <Authors {...props}></Authors>
    </>
  );
};

export default AuthorsList;
