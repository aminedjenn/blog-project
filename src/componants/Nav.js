import React from "react";
import styled from "@emotion/styled";

const StyledNav = styled.nav`
  display: flex;
  border-bottom: 3px solid grey;
  padding: 20px;
`;
const StyledNavBrand = styled.li`
  flex: 1 1 90%;
  list-style-type: none;
  font-size: 1.5em;
  & > a {
    text-decoration: none;
  }
`;
const StyledLi = styled.li`
  list-style-type: none;
  flex: 1 1 10%;
`;

export const NavBrand = (props) => {
  return <StyledNavBrand {...props}></StyledNavBrand>;
};

export const Nav = (props) => {
  const { children } = props;
  return <StyledNav>{children}</StyledNav>;
};

export const Li = (props) => {
  const { children } = props;
  return <StyledLi>{children}</StyledLi>;
};
