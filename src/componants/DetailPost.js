import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import styled from "@emotion/styled";
import dateFormat from "dateformat";
import ReactMarkdown from "react-markdown";
import axios from "axios";

import { StyledSpan } from "./Post";
import { Form } from "./Form";
import Comments from "./Comments";
import { getRequest } from "../axios/axios";

const StyledSpanItalic = styled(StyledSpan)`
  font-style: italic;
  font-size: 1.1em;
  margin-top: 20px;
`;

const StyledBaseDiv = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const ColDiv = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 50%;
  align-items: start;
  padding: 20px;

  & > div {
  }
`;
const TitleDiv = styled.div`
  & > h1 {
    margin: 0 0 5px 0;
    font-style: italic;
  }
`;

export const DetailPost = (props) => {
  const { id } = useParams();
  const [post, setPost] = useState([]);
  const [date, setDate] = useState("");
  const formatedDate = dateFormat(date, "ddd mmm dd yyyy HH:MM:ss");

  useEffect(() => {
    let mounted = true;
    const option = {
      method: "get",
      request: `posts/${id}`,
      params: {},
    };
    getRequest(option).then((res) => {
      if (mounted) {
        const postApi = res.data;
        setPost(postApi);
        setDate(postApi.created_at);
      }
    });
    return () => (mounted = false);
  }, []);
  return (
    <div>
      <StyledBaseDiv>
        <ColDiv>
          <TitleDiv>
            <h1>{post.title}</h1>
            <StyledSpanItalic>{formatedDate}</StyledSpanItalic>
          </TitleDiv>
          <ReactMarkdown>{post.content}</ReactMarkdown>
          <Comments></Comments>
          <Form></Form>
        </ColDiv>
      </StyledBaseDiv>
    </div>
  );
};
