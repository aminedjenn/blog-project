import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getRequest } from "../axios/axios";
import dateFormat from "dateformat";

import { Post } from "./Post";
import { StyledDiv } from "./Home";

const PostsByAuthor = (props) => {
  const { id } = useParams();
  const [postsAuthor, setPostsAuthor] = useState([]);
  const [author, setAuthor] = useState({});
  const option = {
    method: "get",
    request: `posts`,
    params: { limit: 100 },
  };
  const option2 = {
    method: "get",
    request: `authors/${id}`,
    params: {},
  };

  useEffect(() => {
    getRequest(option2).then((res) => {
      setAuthor(res.data);
    });
    getRequest(option).then((res) => {
      const data = res.data.result;
      setPostsAuthor(() => {
        return data.filter((post) => post.author === Number(id));
      });
    });
  }, []);
  return (
    <StyledDiv>
      <h1>All Post(s) from {author.display_name}</h1>
      <p>{postsAuthor.length ? `${postsAuthor.length}` : "No"} Posts(s).</p>
      {postsAuthor.map((post) => {
        const date = post.created_at;
        const formatedDate = dateFormat(date, "ddd mmm dd yyyy HH:MM:ss");
        return (
          <Post
            key={post.id}
            title={post.title}
            date={formatedDate}
            id={post.id}
          ></Post>
        );
      })}
    </StyledDiv>
  );
};

export default PostsByAuthor;
